﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransMe.Data.DataBaseDictionary
{
    class Word
    {
        public string ForeignWord { get; set; }

        public string NativenWord { get; set; }
    }
}
