﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransMe.Data.DataBaseDictionary
{
    static class DataBase
    {
        static Dictionary<string, string> Dictionaries = new Dictionary< string, string>();
        static DataBase()
        {
            Dictionaries = new Dictionary<string, string> {
                {"Dictionaries","Словарь"},
                {"Challenge","Вызов"},
                {"Expansion","Расширение"},
                {"Insane","Ненормальный"},
                {"Turf","Вышвырнуть"},
                {"Lasts","Продолжается"},
                {"Rookie","Новобранец"},
                {"Deploy","Развертывать"},
                {"Increase","Увеличение"}
            };
        }
    }
}
